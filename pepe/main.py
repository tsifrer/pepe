import glob
import os
from jinja2 import Environment, select_autoescape, FileSystemLoader
from markdown import markdown
import shutil


class Pepe(object):

    def __init__(self):
        super().__init__()

        self.base_dir = os.path.dirname(os.path.realpath(__file__))
        self.theme_dir = os.path.join(self.base_dir, 'theme')
        self.dist_dir = os.path.join(self.base_dir, 'dist')
        self.dist_posts_dir = os.path.join(self.dist_dir, 'posts')
        self.src_posts_dir = os.path.join(self.base_dir, 'posts')

    def _create_dist_directory(self):
        shutil.rmtree(self.dist_dir)

        directories = [
            self.dist_dir,
            self.dist_posts_dir,
        ]
        for directory in directories:
            if not os.path.exists(directory):
                os.mkdir(directory)

    def _get_post_paths(self):
        post_star_path = os.path.join(self.src_posts_dir, '*.md')
        return glob.glob(post_star_path, recursive=False)

    def _create_posts(self):
        post_paths = self._get_post_paths()

        template = self.jinja_env.get_template('post.html')
        for path in post_paths:
            with open(path, 'rb') as post:
                text = post.read().decode('utf-8')
                html = markdown(text)

            response = template.render(post_content=html)

            filename = os.path.basename(path)
            filename = '%s.html' % filename.split('.')[0]
            out_file = os.path.join(self.dist_posts_dir, filename)
            with open(out_file, 'wb') as index_file:
                index_file.write(response.encode('utf-8'))

    def _copy_static(self):
        src_static = os.path.join(self.base_dir, 'theme', 'static')
        dist_static = os.path.join(self.dist_dir, 'static')
        shutil.copytree(src_static, dist_static)

    def main(self):
        self._create_dist_directory()

        template_loader = FileSystemLoader(self.theme_dir, followlinks=True)
        self.jinja_env = Environment(
            loader=template_loader,
            autoescape=select_autoescape(['html'])
        )

        self._create_posts()
        self._copy_static()

if __name__ == '__main__':
    pepe = Pepe()
    pepe.main()
